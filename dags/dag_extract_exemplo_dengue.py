import logging
from datetime import datetime, timedelta

import great_expectations as gx
from airflow.decorators import dag, task, task_group
from airflow.operators.empty import EmptyOperator
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.providers.amazon.aws.operators.s3 import S3CreateBucketOperator
from airflow.utils.trigger_rule import TriggerRule
from great_expectations.exceptions import ValidationError
from requests import get

## Possiveis melhorias

# Utilizar a API do IBGE, https://servicodados.ibge.gov.br/api/docs/, para poder pegar todos municipios


## <---------- Constantes ---------->

DISEASES = ["dengue", "chikungunya", "zika"]
MINIO_S3_CONN_ID = "minio_conn"
MINIO_BUCKET = "disease-{disease}-weekly-csv"
PROCESSED_FILE_NAME = "processed/{disease}-{date_file}.csv"


## <---------- Funcoes ---------->


def _get_week_n_year(date: str):
    """
    Retrieves the ISO week number and year of a given date.

    Args:
    ----
        date (str): A string representing the date in the format 'YYYY-MM-DD'.

    Returns:
    -------
        tuple: A tuple containing the ISO week number and year.

    Raises:
    ------
        AssertionError: If the input date is not a string.
    """
    assert isinstance(date, str)

    date_of_execution = datetime.strptime(date, "%Y-%m-%d")
    date_of_execution -= timedelta(weeks=1.1)  # Past week

    week = date_of_execution.isocalendar()[1]
    year = date_of_execution.year

    assert isinstance(week, int) and week >= 1 and week <= 52
    assert isinstance(year, int)

    return week, year


def _get_filename(disease, date_of_execution):
    """
    Generates a filename based on the disease and date of execution.

    Args:
    ----
        disease (str): The name of the disease.
        date_of_execution (str): A string representing the date in the format 'YYYY-MM-DD'.

    Returns:
    -------
        str: The generated filename.

    """
    week, year = _get_week_n_year(date_of_execution)

    return PROCESSED_FILE_NAME.format(disease=disease, date_file=f"Y{year}W{week:02}")


def _get_disease_data(ibge_uf_geocode: int, disease: str, year_week: int, year: int):
    """
    Retrieves disease data from an API.

    Args:
    ----
        ibge_uf_geocode (int): The IBGE UF geocode.
        disease (str): The name of the disease.
        year_week (int): The ISO week number.
        year (int): The year.

    Returns:
    -------
        str: The data obtained from the API.

    Raises:
    ------
        AssertionError: If the input arguments are not of the expected types.
        KeyError: If the disease is not recognized.
    """
    assert isinstance(ibge_uf_geocode, int)
    assert isinstance(year_week, int)
    assert isinstance(year, int)
    assert disease in DISEASES

    request_api = f"https://info.dengue.mat.br/api/alertcity/?geocode={ibge_uf_geocode}"
    request_api += f"&disease={disease}"
    request_api += "&format=csv"
    request_api += f"&ew_start={year_week}&ey_start={year}"
    request_api += f"&ew_end={year_week}&ey_end={year}"

    return get(request_api).text


def _check_if_bucket_exists(conn_id, bucket_name):
    """
    Checks if a bucket exists in the specified connection.

    Args:
    ----
        conn_id (str): The connection ID.
        bucket_name (str): The name of the bucket.

    Returns:
    -------
        bool: True if the bucket exists, False otherwise.
    """
    return S3Hook(conn_id).check_for_bucket(bucket_name)


def _save_minio(data_string, bucket_name, filename):
    """
    Saves data to MinIO storage.

    Args:
    ----
        data_string (str): The data to be saved.
        bucket_name (str): The name of the bucket.
        filename (str): The name of the file.

    """
    S3Hook(MINIO_S3_CONN_ID).load_string(
        string_data=data_string,
        key=filename,
        bucket_name=bucket_name,
        encoding="utf-8",
        replace=True,
    )


def _get_file_from_minio(bucket_name, filename):
    """
    Retrieves a file from MinIO storage.

    Args:
    ----
        bucket_name (str): The name of the bucket.
        filename (str): The name of the file.

    Returns:
    -------
        str: The downloaded file.
    """
    return S3Hook(MINIO_S3_CONN_ID).download_file(
        bucket_name=bucket_name, key=filename, preserve_file_name=False
    )


# To see more validators, see https://greatexpectations.io/expectations/?viewType=Summary
def _validate_data(csv_path):
    """
    Validates the data in a CSV file.

    This function validates the data in a CSV file located at the given path. It checks for
    specific criteria such as non-null values in certain columns and ensures that there is only
    one week worth of data. It returns True if the data passes validation, and False otherwise.

    Args:
    ----
        csv_path (str): The path to the CSV file.

    Returns:
    -------
        bool: True if the data passes validation, False otherwise.
    """
    context = gx.get_context()
    data_validator = context.sources.pandas_default.read_csv(csv_path)
    columns_to_not_be_null = [
        "data_iniSE",
        "SE",
        "casos",
        "p_rt1",
        "nivel",
    ]
    for column in columns_to_not_be_null:
        data_validator.expect_column_values_to_not_be_null(column)

    data_validator.expect_table_row_count_to_be_between(
        min_value=1, max_value=1
    )  # Expected to have only 1 week worth of data

    result = data_validator.validate()
    logging.info(result.get_failed_validation_results())

    return result.success


DEFAULT_ARGS = {
    "owner": "Paulo",
    "depends_on_past": False,
    "email_on_failure": False,
    "email_on_retry": False,
}


@dag(
    default_args=DEFAULT_ARGS,
    schedule="@weekly",
    start_date=datetime(2024, 1, 1),
    catchup=True,
    doc_md=__doc__,
    tags=["dengue", "extraction"],
    max_active_runs=1,
)
def extract_data_dengue():
    start = EmptyOperator(task_id="start")
    end = EmptyOperator(task_id="end")
    date_of_execution = "{{ ds }}"

    for disease in DISEASES:

        @task_group(group_id=f"process_{disease}")
        def process_disease(date, disease):
            @task.branch
            def check_bucket(bucket_name, current_disease):
                if not _check_if_bucket_exists(MINIO_S3_CONN_ID, bucket_name=bucket_name):
                    return [
                        f"process_{current_disease}.create_bucket",
                        f"process_{current_disease}.get_data_for_{current_disease}",
                    ]
                return f"process_{current_disease}.get_data_for_{current_disease}"

            @task(task_id=f"get_data_for_{disease}", trigger_rule=TriggerRule.NONE_FAILED)
            def get_data(current_disease: str, date_of_execution: str, bucket_name: str):
                assert isinstance(current_disease, str)
                assert isinstance(bucket_name, str)

                week, year = _get_week_n_year(date_of_execution)

                _save_minio(
                    _get_disease_data(
                        ibge_uf_geocode=5300108,
                        disease=current_disease,
                        year_week=week,
                        year=year,
                    ),
                    bucket_name=bucket_name,
                    filename=_get_filename(current_disease, date_of_execution),
                )

            @task
            def validate_data(current_disease, bucket_name, date_of_execution):
                filename = _get_filename(disease=current_disease, date_of_execution=date_of_execution)
                if not _validate_data(_get_file_from_minio(bucket_name=bucket_name, filename=filename)):
                    S3Hook(MINIO_S3_CONN_ID).delete_objects(bucket=bucket_name, keys=filename)
                    raise ValidationError(f"Deleted {filename} because it failed the data validation.")

            get_data_task = get_data(
                current_disease=disease,
                date_of_execution=date,
                bucket_name=MINIO_BUCKET.format(disease=disease),
            )
            check_bucket_task = check_bucket(
                bucket_name=MINIO_BUCKET.format(disease=disease), current_disease=disease
            )

            create_bucket_task = S3CreateBucketOperator(
                task_id="create_bucket",
                aws_conn_id=MINIO_S3_CONN_ID,
                bucket_name=MINIO_BUCKET.format(disease=disease),
            )

            check_bucket_task >> create_bucket_task >> get_data_task
            (
                check_bucket_task
                >> get_data_task
                >> validate_data(
                    current_disease=disease,
                    bucket_name=MINIO_BUCKET.format(disease=disease),
                    date_of_execution=date,
                )
            )

        start >> process_disease(date=date_of_execution, disease=disease) >> end


extract_data_dengue()
