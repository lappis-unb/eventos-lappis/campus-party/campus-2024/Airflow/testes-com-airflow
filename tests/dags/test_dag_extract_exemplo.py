from datetime import datetime
from unittest.mock import MagicMock

import pytest

from dags.dag_extract_exemplo_dengue import (
    DISEASES,
    _check_if_bucket_exists,
    _get_disease_data,
    _get_filename,
    _get_week_n_year,
    _validate_data,
)


@pytest.mark.parametrize(
    "date, expected_week, expected_year",
    [
        ("2024-03-28", 12, 2024),
        ("2024-01-01", 51, 2023),
        ("2024-12-31", 52, 2024),
    ],
)
def test_valid_dates(date, expected_week, expected_year):
    print(_get_week_n_year(date))
    assert _get_week_n_year(date) == (expected_week, expected_year)


@pytest.mark.parametrize(
    "invalid_input",
    [
        123,
        None,
        True,
    ],
)
def test_invalid_input(invalid_input):
    with pytest.raises(AssertionError):
        _get_week_n_year(invalid_input)


@pytest.mark.parametrize(
    "invalid_date",
    [
        datetime(2024, 3, 1),  # Current date is used as an example
        "invalid_date_format",
    ],
)
def test_invalid_week_range(invalid_date):
    with pytest.raises((AssertionError, ValueError)):
        _get_week_n_year(invalid_date)


possible_dates = ["2024-01-01", "2024-03-28", "2024-12-31"]


@pytest.mark.parametrize("disease", DISEASES)
@pytest.mark.parametrize("date_of_execution", possible_dates)
def test_filename_generation_with_possible_diseases_and_dates(disease, date_of_execution):
    week, year = _get_week_n_year(date_of_execution)
    expected_filename = f"processed/{disease}-Y{year}W{week:02}.csv"
    assert _get_filename(disease, date_of_execution) == expected_filename


@pytest.mark.parametrize(
    "invalid_date_format",
    [
        "2024/01/01",  # Invalid format
        "2024-01-32",  # Invalid day
        "2024-13-01",  # Invalid month
    ],
)
def test_filename_generation_invalid_date_format(invalid_date_format):
    with pytest.raises(ValueError):
        _get_filename("dengue", invalid_date_format)


@pytest.fixture
def mock_requests_get(mocker):
    return mocker.patch("dags.dag_extract_exemplo_dengue.get")


class ResponseMock:  # noqa: D101
    def __init__(self, text) -> None:
        self.text = text


@pytest.mark.parametrize(
    "ibge_uf_geocode, disease, year_week, year, expected_data",
    [
        (123, "dengue", 12, 2024, "Mocked data for testing"),
        (456, "chikungunya", 20, 2023, "Another mocked data"),
        (789, "zika", 30, 2022, "Yet another mocked data"),
        # Add more test cases
    ],
)
def test_valid_arguments(mock_requests_get, ibge_uf_geocode, disease, year_week, year, expected_data):
    # Mocking the 'get' function
    mock_requests_get.return_value = ResponseMock(expected_data)

    assert _get_disease_data(ibge_uf_geocode, disease, year_week, year) == expected_data


class S3HookMock:  # noqa: D101
    def __init__(self, bucket_exists):
        self.bucket_exists = bucket_exists

    def check_for_bucket(self, bucket_name):
        return self.bucket_exists


@pytest.mark.parametrize(
    "conn_id, bucket_name, bucket_exists",
    [
        ("my_s3_connection", True, True),
        ("my_s3_connection", False, False),
        # Add more test cases as needed
    ],
)
def test_bucket_existence_check(conn_id, bucket_name, bucket_exists, mocker):
    # Create a mock S3Hook instance with the desired bucket existence status
    s3_hook_mock = S3HookMock(bucket_exists)

    # Mock the S3Hook instantiation
    mocker.patch("dags.dag_extract_exemplo_dengue.S3Hook", return_value=s3_hook_mock)

    # Call the function being tested
    assert _check_if_bucket_exists(conn_id, bucket_name) == bucket_exists


@pytest.fixture
def mock_gx_context(mocker):
    # Mocking the gx.get_context() function
    context_mock = MagicMock()
    mocker.patch("dags.dag_extract_exemplo_dengue.gx.get_context", return_value=context_mock)
    return context_mock


def test_valid_data(mock_gx_context):
    csv_path = "valid_data.csv"

    # Mocking the data validator
    data_validator_mock = MagicMock()
    data_validator_mock.expect_column_values_to_not_be_null.return_value = True
    data_validator_mock.expect_table_row_count_to_be_between.return_value = True
    data_validator_mock.validate.return_value = MagicMock(success=True)

    mock_gx_context.sources.pandas_default.read_csv.return_value = data_validator_mock

    assert _validate_data(csv_path)
